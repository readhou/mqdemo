package com.example.mq.controller;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * @author Houjk
 * @version 1.0
 * @date 2022/10/4 20:58
 * @description
 */
@Controller("/test")
public class MqUtil {


    @Autowired
    private RabbitTemplate rabbitTemplate;



    /**
     * 动态路由 或者 订阅  topic
     * @return
     */
    @ResponseBody
    @GetMapping("/e")
    public String e(){
        // 发送
        rabbitTemplate.convertAndSend("topics","user.save","user.save 消息");
        rabbitTemplate.convertAndSend("topics","user.update","user.update 消息");
        rabbitTemplate.convertAndSend("topics","order.update","order.update 消息");
        return "ok";
    }
//    /**
//     * 路由 route
//     * @return
//     */
//    @ResponseBody
//    @GetMapping("/d")
//    public String d(){
//        // 发送
//        rabbitTemplate.convertAndSend("directs","info","发送info的key的路由信息");
//        rabbitTemplate.convertAndSend("directs","error","发送error的key的路由信息");
//        rabbitTemplate.convertAndSend("directs","warn","发送warn的key的路由信息");
//        return "ok";
//    }
//    /**
//     * 广播 fanout
//     * @return
//     */
//    @ResponseBody
//    @GetMapping("/c")
//    public String c(){
//        // 发送
//        rabbitTemplate.convertAndSend("logs","","广播");
//        return "ok";
//    }

//    /**
//     * 一对多work模型
//     * @return
//     */
//    @ResponseBody
//    @GetMapping("/b")
//    public String b(){
//        // 发送
//        HashMap<String, String> map = new HashMap<>();
//        map.put("out_trade_no","666");
//        rabbitTemplate.convertAndSend("work",map);
//        return "ok";
//    }


//    /**
//     * 一对一
//     * @return
//     */
//    @ResponseBody
//    @GetMapping("/a")
//    public String test(){
//        // 发送
//        rabbitTemplate.convertAndSend("hello","你好mq");
//        return "ok";
//    }


}
