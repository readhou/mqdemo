//package com.example.mq.controller;
//
//import org.springframework.amqp.rabbit.annotation.Queue;
//import org.springframework.amqp.rabbit.annotation.RabbitHandler;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import javax.annotation.PostConstruct;
//
///**
// * @author Houjk
// * @version 1.0
// * @date 2022/10/4 21:18
// * @description
// */
//
///**
// * durable 是否持久化
// * autodel 是否自动删除
// */
//@RabbitListener(queuesToDeclare = @Queue(value = "hello"))
//@Component
//public class xfzmq{
//
//    @RabbitHandler
//    public void xfz(String mesg){
//        System.out.println(mesg);
//    }
//
//}