package com.example.mq.controller;

import com.alibaba.fastjson.JSON;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * durable 是否持久化
 * autodel 是否自动删除
 */

@Component
public class xfzmqfanout {

    @RabbitListener(bindings = {
            @QueueBinding(value = @Queue,//不指定名称 使用临时队列
                    exchange = @Exchange(value = "logs",type = "fanout")// 绑定的交换机
            )
    })
    public void xfz1(String message){
        System.out.println("1"+message);
    }
    @RabbitListener(bindings = {
            @QueueBinding(value = @Queue,//不指定名称 使用临时队列
                    exchange = @Exchange(value = "logs",type = "fanout")// 绑定的交换机
            )
    })
    public void xfz2(String message){
        System.out.println("2"+message);
    }


}
