package com.example.mq.controller;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author Houjk
 * @version 1.0
 * @date 2022/10/4 22:02
 * @description
 */
@Component
public class xfzmqroute {

    @RabbitListener(bindings = {
            @QueueBinding(value = @Queue,exchange = @Exchange(value = "directs",type = "direct"),// 指定交换机
            key = {"info","error"})
    })
    public void test1(String msg){
        System.out.println("info"+"error"+msg);
    }

    @RabbitListener(bindings = {
            @QueueBinding(value = @Queue,exchange = @Exchange(value = "directs",type = "direct"),// 指定交换机
                    key = {"error","warn"})
    })
    public void test2(String msg){
        System.out.println("error"+"warn"+msg);
    }
}
