package com.example.mq.controller;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * durable 是否持久化
 * autodel 是否自动删除
 */

@Component
public class xfzmqtopic {


    /**
     * 匹配模式 * 代表一个单词 #一个或者多个
     * @param message
     */
    @RabbitListener(bindings = {
            @QueueBinding(value = @Queue,//不指定名称 使用临时队列
                    exchange = @Exchange(value = "topics",type = "topic"),// 绑定的交换机
                    key = {"user.*"}
            )
    })
    public void xfz1(String message){
        System.out.println("1"+message);
    }
    @RabbitListener(bindings = {
            @QueueBinding(value = @Queue,//不指定名称 使用临时队列
                    exchange = @Exchange(value = "topics",type = "topic"),// 绑定的交换机
                    key = {"user.save","order.*"}
            )
    })
    public void xfz2(String message){
        System.out.println("2"+message);
    }
}
