//package com.example.mq.controller;
//
//import com.alibaba.fastjson.JSON;
//import org.springframework.amqp.rabbit.annotation.Queue;
//import org.springframework.amqp.rabbit.annotation.RabbitHandler;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.amqp.support.converter.MessageConversionException;
//import org.springframework.stereotype.Component;
//
//import java.util.Map;
//
///**
// * durable 是否持久化
// * autodel 是否自动删除
// */
//@RabbitListener(queuesToDeclare = @Queue(value = "work"))
//@Component
//public class xfzmqwork {
//
//    @RabbitHandler
//    public void xfz(String message){
//        try { // try 住语句块 抛出致命异常
//            //1.获取字符串转成map
//            Map<String, String> map = JSON.parseObject(message, Map.class);
//            String out_trade_no = map.get("out_trade_no");
//             System.out.println("11 " +out_trade_no);
//        } catch (Exception e) {
//            e.printStackTrace();
//            // todo  解决消息队列重复试错的bug 抛出一个致命异常就会抛弃消费这个消息
//            throw new MessageConversionException("消息消费失败，移出消息队列，不再试错");
//        }
//    }
//    @RabbitHandler
//    public void xfz2(String message){
//        try { // try 住语句块 抛出致命异常
//            //1.获取字符串转成map
//            Map<String, String> map = JSON.parseObject(message, Map.class);
//            String out_trade_no = map.get("out_trade_no");
//            System.out.println("22" +out_trade_no);
//        } catch (Exception e) {
//            e.printStackTrace();
//            // todo  解决消息队列重复试错的bug 抛出一个致命异常就会抛弃消费这个消息
//            throw new MessageConversionException("消息消费失败，移出消息队列，不再试错");
//        }
//
//    }
//
//}
//
///**
// * @author Houjk
// * @version 1.0
// * @date 2022/10/4 21:18
// * @description
// */
